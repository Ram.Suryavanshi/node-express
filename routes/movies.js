const express = require("express");
const router = express.Router();
const Joi = require("joi");

const movies = [
  { id: 1, name: "Rotti" },
  { id: 2, name: "Hohar" },
  { id: 3, name: "Sushil" },
  { id: 4, name: "Gauri" },
  { id: 5, name: "Shakti" }
];

router.get("/", (req, res) => {
  return res.send(movies);
});

router.get("/:id", (req, res) => {
  const movie = movies.find(m => m.id === parseInt(req.params.id));
  if (!movie)
    return res.status(404).send("Movie with given id is not found!!!");
  return res.send(movie);
});

router.get("/:year/:month", (req, res) => {
  return res.send(req.params);
});

router.post("/", (req, res) => {
  const { error } = validateMovie(req.body);

  if (error) {
    return res.status(400).send(error.details[0].message);
  }
  const movie = {
    id: movies.length + 1,
    name: req.body.name
  };
  movies.push(movie);
  res.send(movies);
});

router.put("/:id", (req, res) => {
  const movie = movies.find(m => m.id === parseInt(req.params.id));
  if (!movie)
    return res.status(404).send("Movie with given id is not found!!!");

  const { error } = validateMovie(req.body);

  if (error) {
    return res.status(400).send(error.details[0].message);
  }

  movie.name = req.body.name;
  res.send(movie);
});

router.delete("/:id", (req, res) => {
  const movie = movies.find(m => m.id === parseInt(req.params.id));
  if (!movie)
    return res.status(404).send("Movie with given id is not found!!!");
  const index = movies.indexOf(movie);
  movies.splice(index, 1);
  res.send(movies);
});

validateMovie = movie => {
  console.log(movie);
  const schema = {
    name: Joi.string()
      .min(3)
      .required()
  };
  return Joi.validate(movie, schema);
};

module.exports = router;
