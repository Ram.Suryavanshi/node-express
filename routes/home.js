const express = require("express");

const router = express.Router();

router.get("/", (req, res) => {
  return res.render("index", {
    title: "Express Node App",
    message: "This is node express app..."
  });
});

module.exports = router;
