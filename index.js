const debug = require("debug")("app:startup");
const config = require("config");
const morgan = require("morgan");
const logger = require("./logger");
const movies = require("./routes/movies");
const home = require("./routes/home");
const express = require("express");
const app = express();

app.use(express.json());
app.use(express.static("public"));
app.use(logger);

app.use("/", home);
app.use("/api/movies", movies);

app.set("view engine", "pug");
app.set("views", "./views");

if (app.get("env") === "development") {
  debug("Logging enabled..");
  app.use(morgan("tiny"));
}

// console.log(`app name:- ${config.get("name")}`);
// console.log(`app name:- ${config.get("mail.host")}`);
// console.log(`app password:- ${config.get("mail.password")}`);

const port = process.env.PORT || 4200;

app.listen(port, () => {
  console.log(`listening on port ${port}....`);
});
